package com.example.emailEditor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class EmailController {

    @GetMapping({"/email"})
    public String email(Model model) { return "email"; }

    @GetMapping({"/loadPageHtmlUrl"})
    public String loadPageHtmlUrl(Model model) {
        return "template-load-page";
    }

    @GetMapping({"/blankPageHtmlUrl"})
    public String blankPageHtmlUrl(Model model) {
        return "template-blank-page";
    }

    @PostMapping({"/sentmail"})
    public String sentmail(@RequestBody String htmlText) {
        System.out.println(htmlText.toString());
        return "email";
    }


}
